Welcome to my MN-Magic repository.


What is it about?
This tool is designed to create a Globaltoken masternode in the easyest way possible.

You will need a Linux 64 Bit VPS.
Recommended is an Ubuntu 16.04 Server with 1Gb Ram or more.

Start your Globaltoken-QT wallet and let it fully sync.

Send yourself 50000 GLT to a new address and wait for 15 confirmations.

Start the MN-Magic tool.

Enter the GLT Address in the first line.
IP		- Line needs your servers IP.
Username	- The username to login on the Server (most cases root)
Password	- the login password for the user on the server

After filling in all required data, press the green button once and watch it happening :)

When the Log tells you to Start the Masternode - just do that :)

Thats all you need to do.

Greetings - Astrali
