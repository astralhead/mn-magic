#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Astrali

 Script Function:
	Setup Masternodes automatically

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Constants.au3>
#include <FileConstants.au3>
#include <Array.au3>
#include <GuiEdit.au3>
#include <File.au3>

local $data
Local $eMsg, $stdout, $stdin


global $address = ""
global $username = ""
global $server = ""
global $port = "22"
global $password = ""
global $pid

global $rpcuser = ""
global $rpcpass = ""

global $path = ""		;globaltoken-qt.exe
global $gltcli = ""		;globaltoken-cli.exe

global $txid 	= ""
global $idx 	= ""
global $privkey = ""

global $mnconfigline = ""



#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=C:\Users\Tester\Desktop\Bitbucket\mn-magic\MN-Magic.kxf
$Form1 = GUICreate("MN-Magic", 765, 651, 261, 196)
$Input1 = GUICtrlCreateInput("Address", 134, 49, 336, 24)	;GLT-Address
$Input2 = GUICtrlCreateInput("IP", 134, 82, 158, 24)		;IP
$Input3 = GUICtrlCreateInput("root", 134, 124, 158, 24)		;user
$Input4 = GUICtrlCreateInput("", 134, 163, 158, 24,$ES_PASSWORD)		;password
$Create = GUICtrlCreateButton("Create Masternode", 513, 42, 197, 40)
GUICtrlSetBkColor(-1, 0x00FF00)
$Label1 = GUICtrlCreateLabel("GLT Address", 32, 49, 84, 20)
$Label2 = GUICtrlCreateLabel("Server IP", 32, 84, 59, 20)
$Label3 = GUICtrlCreateLabel("Username", 32, 126, 67, 20)
$Label4 = GUICtrlCreateLabel("User Password", 32, 165, 96, 20)
$Log = GUICtrlCreateEdit("", 32, 216, 721, 409)
GUICtrlSetData(-1, "Log")
$Input5 = GUICtrlCreateInput("22", 396, 82, 73, 24)
$Label5 = GUICtrlCreateLabel("Server port", 312, 84, 70, 20)
$Status = GUICtrlCreateButton("Check MN Status", 513, 88, 197, 41)
GUICtrlSetBkColor(-1, 0xFFFF00)
$Button1 = GUICtrlCreateButton("(Re)Start MN", 513, 135, 197, 41)
GUICtrlSetBkColor(-1, 0xFF0000)
$Label6 = GUICtrlCreateLabel("Language", 512, 16, 65, 20)
$Language = GUICtrlCreateCombo("Language", 580, 15, 129, 25, BitOR($CBS_DROPDOWN,$CBS_AUTOHSCROLL))
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###


;======================== Check for Globaltoken Core wallet and cancel if not available =============================
	  Global $path = _wingetpath("Globaltoken Core")
	  if stringinstr($path,"Autoit") Then
		 Msgbox(0,"ERROR","Missing Globaltoken Core wallet. Please start globaltoken-qt.exe")
		 Exit
	  EndIf
	  _GUICtrlEdit_AppendText($log,@CRLF & "Found Globaltoken Core wallet installed at: " & $Path & @CRLF)
;====================================================================================================================


;========================================== check for globaltoken.conf ==============================================
Local $sFilePath = RegRead("HKEY_CURRENT_USER\SOFTWARE\Globaltoken\Globaltoken-Qt", "strDataDir")
if not @error Then
   global $regpath = $sFilePath
Else
   global $regpath = @AppDataDir&"\Globaltoken"
EndIf


if FileExists($regpath&"\globaltoken.conf") Then
   _GUICtrlEdit_AppendText($log,"Found globaltoken.conf lets proceed "& @CRLF)



   Local $configFile = Fileopen($regpath&"\globaltoken.conf")
   global $maxlines = _FileCountLines ( $regpath&"\globaltoken.conf" )

;=========================grab the rpcuser from config=======================
		 $notfound = 1
		 $line = 1
		 While $notfound = 1
			$linetext = filereadline($configFile,$line)
			if Stringinstr($linetext,"rpcuser") Then
			   $tmp = Stringsplit($linetext,"=")
			   $rpcuser = $tmp[2]
			   ExitLoop
			EndIf

			if $line < $maxlines Then
			   $line = $line +1
			else
			   _GUICtrlEdit_AppendText($log,@tab & @tab & "Config file there but no RPCUSER defined! Please enter Manually"& @CRLF)
			   _GUICtrlEdit_AppendText($log,@tab & @tab & "rpcuser=yourusername"& @CRLF)
			   Run("notepad.exe " & $regpath&"\globaltoken.conf")
			   sleep(2000)
			   Msgbox(48,"ERROR","Globaltoken.conf present, but missing rpcuser=USERNAME please add manually or delete whole config.")
			   Exit
			EndIf
		 WEnd
;======================grab the rpcpassword from config========================
		 $notfound = 1
		 $line = 1
		 While $notfound = 1
			$linetext = filereadline($configFile,$line)
			if Stringinstr($linetext,"rpcpassword") Then
			   $tmp = Stringsplit($linetext,"=")
			   $rpcpass = $tmp[2]
			   ExitLoop
			EndIf


			if $line < $maxlines  Then
			   $line = $line +1
			Else
			   _GUICtrlEdit_AppendText($log,@tab & @tab & "Config file there but no RPCUSER defined! Please enter Manually"& @CRLF)
			   _GUICtrlEdit_AppendText($log,@tab & @tab & "rpcpassword=yourpassword"& @CRLF)
			   Run("notepad.exe " & $regpath&"\globaltoken.conf")
			   sleep(2000)
			   Msgbox(48,"ERROR","Globaltoken.conf present, but missing rpcpassword=SECUREPASSWORD please add manually or delete whole config.")
			   Exit
			EndIf
		 WEnd
   Fileclose($configFile)
   _GUICtrlEdit_AppendText($log,"Using " & $rpcuser & " with password " & $rpcpass & " to connect to local wallet" & @CRLF)

Else
   ;====================================== Missing Config! lets do this shit ========================================
   _GUICtrlEdit_AppendText($log,"We need a globaltoken.conf lets create a basic config! "& @CRLF)
	   Local $configFile = Fileopen($regpath&"\globaltoken.conf",$FO_APPEND)
	   If $configFile = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
        Return False
    EndIf

	$pwd = ""
	$userRND = ""
	  Dim $aSpace[3]
		 $digits = 15
		 For $i = 1 To $digits
			$aSpace[0] = Chr(Random(65, 90, 1)) ;A-Z
			$aSpace[1] = Chr(Random(97, 122, 1)) ;a-z
			$aSpace[2] = Chr(Random(48, 57, 1)) ;0-9
			$pwd &= $aSpace[Random(0, 2, 1)]
		 Next

	  Dim $aSpace[3]
		 $digits = 5
		 For $i = 1 To $digits
			$aSpace[0] = Chr(Random(65, 90, 1)) ;A-Z
			$aSpace[1] = Chr(Random(97, 122, 1)) ;a-z
			$aSpace[2] = Chr(Random(48, 57, 1)) ;0-9
			$userRND &= $aSpace[Random(0, 2, 1)]
		 Next


	   Filewriteline($configFile,"rpcuser=gltuser" & $userRND)
	   Filewriteline($configFile,"rpcpassword="& $pwd)
	   Filewriteline($configFile,"rpcallowip=127.0.0.1")
	   Filewriteline($configFile,"server=1")
	   Filewriteline($configFile,"rpcport=9320")
	   Fileclose($configFile)
   _GUICtrlEdit_AppendText($log,"Config created! "& @CRLF)
   $rpcuser="gltuser" & $userRND
   $rpcpass=$pwd
   _GUICtrlEdit_AppendText($log,"Using " & $rpcuser & " with password " & $rpcpass & " to connect to local wallet" & @CRLF)
   _GUICtrlEdit_AppendText($log,"lets restart the wallet. "& @CRLF)
   WinClose ( "Globaltoken Core" )
   While ProcessExists($GLTPID)
	  sleep(100)
   WEnd
   _GUICtrlEdit_AppendText($log,"Shutdown Complete! Restarting the Wallet."& @CRLF)
   Run($path) ;starting the Globaltoken executable :)

   Opt("WinTitleMatchMode", 3) ;1=start, 2=subStr, 3=exact, 4=advanced, -1 to -4=Nocase
     While not Winexists("Globaltoken Core - Wallet ")
		sleep(2000)
		Tooltip("Waiting for started Wallet")
	 WEnd
	  Opt("WinTitleMatchMode", 1) ;1=start, 2=subStr, 3=exact, 4=advanced, -1 to -4=Nocase
	  Tooltip("")
   _GUICtrlEdit_AppendText($log,"MN-Magic ready for Setup!"& @CRLF)
EndIf
;======================================================= MN-Magic Initialised - lets start the GUI ===========================================


While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
	Case $Create
	  readcredentials()
	  collectlocaldata()
	  CreateMNLine()
	  sshlogin()
	  installmn($mnconfigline)
	  restartwallet()
   Case $Status
	  readcredentials()
	  Getstatus()
   Case $Button1
	  readcredentials()
	  RestartMN()
		Case $GUI_EVENT_CLOSE
			Exit

	EndSwitch
WEnd




Func Getstatus()
   sshlogin()
;==============================================================checking mn status======================================================================
	  Stdinwrite($PID,"~/globaltoken/bin/globaltoken-cli masternode status" & @CRLF) 										;Check MN Status
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"ASKED FOR MN STATUS" & @CRLF)
;======================================================================================================================================================
;============================================================checking for MN status ==========================================================================================
	  $Errlog = StdERRRead($pid,true)
	  $Outlog = StdOUTRead($pid,true)
		 while stringinstr($Outlog,"{") == 0								;waiting for Reply
			   Sleep(100)
				  $Errlog = StdERRRead($pid,true)
				  $Outlog = StdOUTRead($pid,true)
		 wend
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"STATUS FOUND" & @CRLF)
$Outputarray = Stringsplit($Outlog,"{}")
_GUICtrlEdit_AppendText($log,$Outputarray[2] & @CRLF)
;======================================================================================================================================================
Stdinwrite($PID,"exit" & @CRLF)
_GUICtrlEdit_AppendText($log,@TAB & @TAB & 							"CLOSED CONNECTION TO SERVER" & @CRLF & @CRLF)
;======================================================================================================================================================
EndFunc


Func Restartmn()
   sshlogin()
;==============================================================restarting masternode===================================================================
	  Stdinwrite($PID,"~/globaltoken/bin/globaltoken-cli stop" & @CRLF) 										;stop MN
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"REQUESTED MN STOP BY CLI" & @CRLF)
;======================================================================================================================================================
sleep(2000)
	  Stdinwrite($PID,"pkill globaltokend" & @CRLF) 										;stop MN
;============================================================killing process if still running after 2 seconds==========================================
sleep(500)
	  Stdinwrite($PID,"~/globaltoken/bin/globaltokend" & @CRLF) 										;start node
;========================================================================wallet started again==========================================================
	  $Errlog = StdERRRead($pid,true)
	  $Outlog = StdOUTRead($pid,true)
		 while stringinstr($Outlog,"starting") == 0								;waiting for password prompt
			   Sleep(100)
				  $Errlog = StdERRRead($pid,true)
				  $Outlog = StdOUTRead($pid,true)
		 wend
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"WALLET RESTARTED" & @CRLF)
;======================================================================================================================================================
Stdinwrite($PID,"exit" & @CRLF)
_GUICtrlEdit_AppendText($log,@TAB & @TAB & 							"CLOSED CONNECTION TO SERVER" & @CRLF & @CRLF)
;======================================================================================================================================================
EndFunc



Func restartwallet()
   WinClose ( "Globaltoken Core" )
   While ProcessExists($GLTPID)
	  sleep(100)
   WEnd
   _GUICtrlEdit_AppendText($log,"Shutdown Complete! Restarting the Wallet."& @CRLF)
   Run($path) ;starting the Globaltoken executable :)
_GUICtrlEdit_AppendText($log,"All done! Please wait for full Wallet start, go to Masternode Tab" & @CRLF)
_GUICtrlEdit_AppendText($log,"and start your new Masternode! You can close the MN-Magic Tool now." & @CRLF)
EndFunc


Func CreateMNLine()
   Local  $MNconfig =  Fileopen($regpath&"\masternode.conf")
   global $maxlines = _FileCountLines ($regpath&"\masternode.conf")

		 $notfound 	= 1
		 $line 		= 1
		 $mncount 	= 0
		 While $notfound = 1
			$linetext = filereadline($MNconfig,$line)
			if Stringinstr($linetext,"#") Then
			   $line = $line +1
			Else
			   $mncount = $mncount +1
			   $line = $line +1
			EndIf

			if $line = $maxlines +1 Then
			   _GUICtrlEdit_AppendText($log,"Found " & $mncount & " masternodes in config" & @CRLF)
			   exitloop
			EndIf
		 WEnd

Fileclose($MNconfig)
   Local $MNconfig = Fileopen($regpath&"\masternode.conf",1)
;=================FIX @CRLF in MN.conf
Global $s_EndChars = @CRLF
FileSetPos($MNconfig, -StringLen($s_EndChars), 1)
If FileRead($MNConfig) <> $s_EndChars Then FileWrite($MNConfig, $s_EndChars)
;=========================================================================
   $idx = StringstripCR($idx)
	  Filewriteline($MNconfig,"MN" & $mncount + 1 & " " & $server & ":9319 "& $privkey & $txid & " " & $idx)
   Fileclose($MNconfig)
_GUICtrlEdit_AppendText($log,"Added MN" & $mncount + 1 & " to masternode.conf - let´s restart the Wallet and install it to your server!" & @CRLF)
$mnconfigline = "MN" & $mncount + 1 & " " & $server & ":9319 "& $privkey & $txid & " " & $idx
EndFunc


Func collectlocaldata()
   $gltcli = Stringreplace($path,"globaltoken-qt.exe","globaltoken-cli.exe")
   if not FileExists($gltcli) Then
	   $gltcli = Stringreplace($gltcli,"globaltoken-cli.exe",'daemon\globaltoken-cli.exe')
   EndIf
;================================================ Get TXHash and Output Index ==========================================================================
   $clipid = Run($gltcli & " -rpcuser=" & $rpcuser & " -rpcpassword=" & $rpcpass & " masternode outputsbyaddress " & $address , "" , @SW_HIDE , $STDERR_CHILD + $STDOUT_CHILD + $STDIN_CHILD)

   Local $clioutput = StdOUTRead($clipid,true) ;read console output
   Local $clierror = StdERRRead($clipid,true) ;read console output

   while stringinstr($clioutput,"}") < 1 or stringinstr($clierror,"error") < 1								;waiting for output
			   Sleep(100)
			   tooltip("Waiting for CLI answer")
			   $clioutput &= StdOUTRead($clipid) 										; Read the Stdout stream of the PID returned by Run.
			   $clierror &= StdERRRead($clipid,true) ;read console output
   if Stringinstr($clierror,"error") then
	  Msgbox(48,"ERROR", "Please send yourself 50000 GLT and use this address!")
	  exit
   EndIf
   if Stringinstr($clioutput,"}") then
	  exitloop
   EndIf
   wend
   tooltip("")
   if Stringinstr($clioutput,"}") then
   $clioutput = stringreplace($clioutput,"{","")
   $clioutput = stringreplace($clioutput,"}","")
   $clioutput = stringreplace($clioutput,'"',"")
   $splitout = Stringsplit($clioutput,",:")
$txid 	= $splitout[3]
$idx 	= $splitout[5]
$idx = StringstripWS($idx,8)

_GUICtrlEdit_AppendText($log, "Found TXID" & @tab & @tab & ": " & $txid & @CRLF)
_GUICtrlEdit_AppendText($log, "Found Output Index" & @tab & @tab & ": " & $idx & @CRLF)
   EndIf
;================================================ Get Masternode Privatekey ==========================================================================

   $clipid = Run($gltcli & " -rpcuser=" & $rpcuser & " -rpcpassword=" & $rpcpass & " masternode genkey ", "" , @SW_HIDE , $STDERR_CHILD + $STDOUT_CHILD + $STDIN_CHILD)

   Local $clioutput = StdOUTRead($clipid,true) ;read console output


   while stringlen($clioutput) < 1								;waiting for output
			   Sleep(100)
			   tooltip("Waiting for CLI answer")
			   $clioutput &= StdOUTRead($clipid) 										; Read the Stdout stream of the PID returned by Run.
   wend
   tooltip("")
$privkey = $clioutput
$privkey = StringstripWS($privkey,8)
_GUICtrlEdit_AppendText($log, "Generated Masternode Privatekey" & ": " & $privkey & @CRLF)
EndFunc


Func readcredentials()
$address 	= guictrlread($input1)
$server 	= guictrlread($input2)
$username 	= guictrlread($input3)
$password 	= guictrlread($input4)
$port 		= guictrlread($Input5)
EndFunc


Func SSHlogin()
;run the plink executable to establish connection
Global $pid = Run(@ScriptDir & "\plink.exe " & " " & $username & "@" & $server & " -P " & $port ,@ScriptDir , @SW_HIDE , $STDOUT_CHILD + $STDERR_CHILD + $STDIN_CHILD)
Sleep(500)
Global $Errlog = StdERRRead($pid,true)
Global $Outlog = StdOUTRead($pid,true)
	  While stringinstr($Errlog,"cache") = 0 and stringinstr($Outlog,"password") = 0
	  $Errlog = StdERRRead($pid,true)
	  $Outlog = StdOUTRead($pid,true)
	  sleep(50)
	  WEnd
						_GUICtrlEdit_AppendText($log, @CRLF)
						_GUICtrlEdit_AppendText($log, @TAB & @TAB & "CONNECTION ESTABLISHED" & @CRLF)
;======================================================================================================================================================

;=============================================================Accept hostkey on first connection=======================================================
			if stringinstr($Errlog,"Store key in cache") > 0 Then 					;if question is found then ..
			_GUICtrlEdit_AppendText($log, @TAB & @TAB & 			"ACCEPTING HOSTKEY" & @CRLF)
			Stdinwrite($PID,"y" & @CRLF)											;sending y
			EndIf
;======================================================================================================================================================

;=============================================================waiting for password prompt==============================================================
	  $Errlog = StdERRRead($pid,true)
	  $Outlog = StdOUTRead($pid,true)
		 while stringinstr($Outlog,"password") == 0								;waiting for password prompt
			   Sleep(100)
				  $Errlog = StdERRRead($pid,true)
				  $Outlog = StdOUTRead($pid,true)
		 wend
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"BEING ASKED FOR PASSWORD" & @CRLF)
;======================================================================================================================================================

;==============================================================sending password========================================================================
	   Stdinwrite($PID,$password & @CRLF) 										;after password prompt was found - send password
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"PASSWORD SENT" & @CRLF)
;======================================================================================================================================================

;Msgbox(0,"test","ERR: " & $Errlog & @CRLF & "OUT: " & $Outlog & @CRLF & @ERROR)
;============================================================checking for successful login==========================================================================================
	  $Errlog = StdERRRead($pid,true)
	  $Outlog = StdOUTRead($pid,true)
		 while stringinstr($Outlog,"@") < 0								;waiting for password prompt
			   Sleep(100)
				  $Errlog = StdERRRead($pid,true)
				  $Outlog = StdOUTRead($pid,true)
				  tooltip("ERR: " & $Errlog & @CRLF & "OUT: " & $Outlog & @CRLF & @ERROR)
		 wend
	  _GUICtrlEdit_AppendText($log, @TAB & @TAB & 					"LOGIN SUCCESSFULL" & @CRLF)
;======================================================================================================================================================
;DEBUGSTUFF
		 Stdinwrite($PID,"touch login.complete" & @CRLF) 										;after password prompt was found - send password

EndFunc


Func installmn($mnconfigline)

   Stdinwrite($PID,"rm -rf setup.sh" & @CRLF)								;remove old setup files if present
   Stdinwrite($PID,"wget cryptopowered.club/glt/setup.sh" & @CRLF)			;Download the MN Install script
   _GUICtrlEdit_AppendText($log,@TAB & @TAB & 						"WAITING FOR DOWNLOAD TO COMPLETE" & @CRLF & @CRLF)
   _GUICtrlEdit_AppendText($log,"wget cryptopowered.club/glt/setup.sh" & @CRLF & @CRLF)

while stringinstr($Outlog,"saved") < 0						;waiting for saved setup
	  Sleep(100)
	  $Outlog &= StdOutRead($PID) 									; Read the Stdout stream of the PID returned by Run.
wend
   _GUICtrlEdit_AppendText($log,@TAB & @TAB & 						"DOWNLOAD COMPLETE" & @CRLF & @CRLF)

Stdinwrite($PID,"chmod 777 setup.sh" & @CRLF & @CRLF)
 _GUICtrlEdit_AppendText($log,"chmod 777 setup.sh" & @CRLF)
	  _GUICtrlEdit_AppendText($log,@TAB & @TAB & 					"MADE SCRIPT EXECUTABLE" & @CRLF)
Sleep(500)
_GUICtrlEdit_AppendText($log,@TAB & @TAB & 							"STARTING SETUP SERVERSIDE" & @CRLF & @CRLF )
Stdinwrite($PID,"./setup.sh " & $mnconfigline & @CRLF)
_GUICtrlEdit_AppendText($log,"./setup.sh " & $mnconfigline & @CRLF & @CRLF )

while stringinstr($Outlog,"ALL DONE") < 0						;waiting for saved setup
	  Sleep(200)
	  $Outlog &= StdOutRead($PID) 									; Read the Stdout stream of the PID returned by Run.
wend
_GUICtrlEdit_AppendText($log,@TAB & @TAB & 							"SERVERSIDE SETUP COMPLETE" & @CRLF)
Stdinwrite($PID,"exit" & @CRLF)
_GUICtrlEdit_AppendText($log,@TAB & @TAB & 							"CLOSED CONNECTION TO SERVER" & @CRLF & @CRLF)
EndFunc


Func _WinGetPath($Title="", $strComputer='localhost')
    $win = WinGetTitle($Title)
    $pid = WinGetProcess($win)
global $GLTPID = $pid
   $wbemFlagReturnImmediately = 0x10
   $wbemFlagForwardOnly = 0x20
   $colItems = ""
   $objWMIService = ObjGet("winmgmts:\\" & $strComputer & "\root\CIMV2")
   $colItems = $objWMIService.ExecQuery ("SELECT * FROM Win32_Process WHERE ProcessId = " & $pid, "WQL", _
         $wbemFlagReturnImmediately + $wbemFlagForwardOnly)
   If IsObj($colItems) Then
      For $objItem In $colItems
         If $objItem.ExecutablePath Then Return $objItem.ExecutablePath
      Next
   EndIf
EndFunc


Func Findpath($processname)
   $list = ProcessList($processname)
for $i = 1 to $list[0][0]
    $pid = $list[$i][1]
    Local $hProc = DllCall("kernel32.dll", "int", "OpenProcess", "int", 0x0410, "int", False, "int", $pid)
    If $hProc[0] Then
        Local $stHMod = DllStructCreate("int hMod")
        Local $stCB = DllStructCreate("dword cbNeeded")
        Local $resEnum = DllCall("psapi.dll", "int", "EnumProcessModules", "int", $hProc[0], "ptr", DllStructGetPtr($stHMod), "dword", DllStructGetSize($stHMod), "ptr", DllStructGetPtr($stCB, 1))
        If $resEnum[0] Then
            Local $resPath = DllCall("psapi.dll", "int", "GetModuleFileNameEx", "int", $hProc[0], "int", DllStructGetData($stHMod, 1), "str", "", "dword", 32768)
            MsgBox(0, "PID 2 Path", "" & $Pid & " = " & $resPath[3])
            EndIf
        $stHMod = 0
        $stCB = 0
        DllCall("kernel32.dll", 'int', 'CloseHandle', 'int', $hProc[0])
    EndIf
 Next
 _GUICtrlEdit_AppendText($log,@TAB & @TAB & $resPath & @CRLF)
EndFunc

Exit
